from django.shortcuts import render
from django.views.generic.edit import FormView
from django import forms
from django.core.mail import send_mail
from django.utils.translation import ugettext as _

import sys
#import settings
# Create your views here.
class ContactForm(forms.Form):
    sender = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': _('Email' + ' *')}))
    phone = forms.CharField(widget=forms.EmailInput(attrs={'placeholder': _('Phone' + ' *')}))
    message = forms.CharField(max_length=4096, widget=forms.Textarea(attrs={'placeholder': _('Message' + ' *'), "rows": 10}))

    def send_email(self, instance):      
        send_mail(
            instance.subject, 
            self.cleaned_data['message'], 
            self.cleaned_data['sender'], 
            [instance.email]
        )
        
        #sys.exit()


class ContactView(FormView):
    template_name = 'contact_form.html'
    form_class = ContactForm
    success_url = '/thanks/'
    '''
    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        #form.send_email(self, form)
        return super(ContactView, self).form_valid(form)
    '''