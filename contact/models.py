from django.db import models

from cms.models.pluginmodel import CMSPlugin
 
# 
# 
class Inquiry(CMSPlugin):
    subject = models.CharField(max_length=50, default='Message from site')
    email = models.EmailField()