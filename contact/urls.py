from contact import views
from django.conf.urls import patterns, url

__author__ = 'soloma'


urlpatterns = patterns('',
    url(r'^contact/$', views.ContactView.as_view(), name='contact'),
)