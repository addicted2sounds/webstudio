from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from cms.plugin_base import CMSPluginBase
from contact.views import ContactForm
from django.utils.translation import ugettext as _

from .models import Inquiry

__author__ = 'soloma'

class ContactPlugin(CMSPluginBase):
    """Enables latest event to be rendered in CMS"""
    #model = CMSPlugin
    model = Inquiry
    name = _("Contact Form")
    render_template = "contact_plugin.html"

    def render(self, context, instance, placeholder):  
        request = context['request']
        if request.method == 'POST':
            form = ContactForm(request.POST)
            if form.is_valid():
                form.send_email(instance)
                context.update( {
                    'contact': instance,
                })
        else:
            form = ContactForm()
            context.update({
                'instance': instance,
                'form': form,
            })
        return context          

plugin_pool.register_plugin(ContactPlugin)

