'use strict';
$('#menu-wrap').hide(); 
$(document).ready(function(){

    $('#toggleMenu').on('click', function() {
        $('#menu-wrap').slideUp('slow');
        $('#menu-wrap:hidden').slideDown('slow');
    });

    $("#main-nav a img").mouseover(function() {
    	$(this).attr("src", $(this).data("hover-src"));
    });   

    var navPics = {
    	'promote': [
		    "/static/images/banner-promote-active.png",
		    "/static/images/banner-seo.png"    		
    	],
    	'design': [
		    "/static/images/banner-design-active.png",
		    "/static/images/banner-design.png"    		
    	],
    	'marketing': [
		    "/static/images/banner-marketing-active.png",
		    "/static/images/banner-marketing.png"    		
    	],
    	'smm': [
		    "/static/images/banner-pr-active.png",
		    "/static/images/banner-pr.png"    		
    	],
    } 
    $("#main-nav a").mouseover(function() {
    	var btn = $(this).attr('class');
    	if (navPics[btn] !== undefined)
    		$(this).find('img').attr('src', navPics[btn][0]);
    });    
    $("#main-nav a").mouseout(function() {
    	var btn = $(this).attr('class');
    	if (navPics[btn] !== undefined)
    		$(this).find('img').attr('src', navPics[btn][1]);
	});

    $('.carousel').carousel();
});
